Redhat - CWS-Setup

Ansible playbooks to download and run the script on CWS server.

Roles

Ansible playbooks to download the content from URL as a script and execute in CWS Server. 

Below are the roles used in this playbook

cws-prerequisites -- Installs the basic pre-requisites required for executing this playbook
cws-script -- Download and execute the script on CWS server

The roles are to be executed in the order as per the playbook, as the components are dependant on the availability of each other.

Running Playbook
# ansible-playbooks -i <hosts-file-path> playbooks/cws/cws-setup.yml 

The variable are defined in config.yml file ( URL to download the script and credentials to connect the URL)

Playbook Execution outline:


1. Check the prerequisites ( wget is installed / if not installed, it will install the wget package)

2. Check the download URL and its credentials

3. Downloads the content from URL and writes into frm.sh script file. If we run the playbook again the file content is same it will not download again but will execute the script. if there is any update on source URL (content), it will take backup of existing file and replace the original file(destination) and run the script.

4. The Output of the script will be stored under /opt/frm.log files

